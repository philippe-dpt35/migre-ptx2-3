# Scripts de migration de PrimTux 2 et 3

Ces scripts permettent de migrer les versions 2 et 3 de PrimTux vers la version 4

Sont concernées par cette migration:

- PrimTux2 i386
- PrimTux2 amd64
- PrimTux2 RPi
- PrimTux3 i386
- PrimTux3 amd64
- PrimTux3 RPi

La migration effectue les changements suivants :

- barre latérale gauche de lancement des applications
- fonds d'écran mothsart
- handymenus réécris
- Libreoffice des écoles remanié
et diverses autres améliorations.

## Utilisation

Décompresser l'archive **migre-ptx2-3** dans le répertoire de son choix.
Ouvrir un terminal dans ce répertoire, et saisir :
```
sudo ./ptx2-3-vers-4.sh
```
Les scripts python de conversion des fichiers de configuration des handymenus ont été écrits par mothsart sous licence libre et ses sources sont accessibles sur [Framagit](https://framagit.org/mothsart/scripts_migration).
