#!/usr/bin/python
# -*- coding:Utf-8 -*- 

import sys
import os
import traceback
import pickle
import io
import yaml
from shutil import copyfile

def coloring(message, ok=False):
    color = '\033[91m'
    if ok:
        color = '\033[92m'
    print(
        color
        + message
        + '\033[0m'
    )

def convert_config(in_path, out_path, skel_path=None, verbose=False):
    """ 1. Convertis les fichiers .conv provenant des 4 sessions par défaut
        2. Enregistre le résultat dans /etc/handymenu/handymenu-{lasession}.yaml
        3. Enregistre le résultat dans le squelette de la session soit dans /etc/skel/handymenu.yaml (par exemple pour le prof)
    """
    try:
        with open(in_path, 'rb') as pkl:
            try:
                config = pickle.load(pkl)
            except:
                try:
                    config = pickle.load(pkl, encoding="utf-8")
                except:
                    coloring(
                        "Impossible de décoder le fichier %s" % in_path
                    )
                    if verbose:
                        traceback.print_exc()
                    return
    except:
        coloring(
            "Impossible d'ouvrir fichier %s" % in_path
        )
        if verbose:
            traceback.print_exc()
        return
    try:
        with io.open(out_path, 'w', encoding='utf8') as outfile:
            try:
                yaml.safe_dump(
                    config,
                    outfile,
                    default_flow_style=False,
                    allow_unicode=True
                )
                coloring("%s créé." % out_path, True)
            except:
                coloring("Impossible d'encoder le fichier %s" % out_path)
                if verbose:
                    traceback.print_exc()
                return
    except:
        coloring(
            "Impossible d'enregistrer le fichier %s" % out_path
        )
        if verbose:
            traceback.print_exc()
    if skel_path == None:
        return
    try:
        with io.open(skel_path, 'w', encoding='utf8') as outfile:
            try:
                yaml.safe_dump(
                    config,
                    outfile,
                    default_flow_style=False,
                    allow_unicode=True
                )
                coloring("%s créé." % skel_path, True)
            except:
                coloring("Impossible d'encoder le fichier %s" % skel_path)
                if verbose:
                    traceback.print_exc()
                return
    except:
        coloring(
            "Impossible d'enregistrer le fichier %s" % skel_path
        )
        if verbose:
            traceback.print_exc()

if __name__ == "__main__":
    verbose = False
    if (
       len(sys.argv) > 1 
       and (sys.argv[1] == "-v" or sys.argv[1] == "--verbose")
    ):
        verbose = True
    coloring(
        "Etape 1 : Migration des sessions mini, super, maxi et prof",
        True
    )
    sessions = ['01-mini', '02-super', '03-maxi', 'administrateur']
    for session in sessions:
        skel_session = session.replace('01', '').replace('02', '').replace('03', '').replace('administrateur', '')
        if session == 'administrateur':
            session = 'prof'
        convert_config(
            "/home/" + session + "/handymenu.conf",
            "/etc/handymenu/handymenu-" + session + ".yaml",
            "/etc/skel" + skel_session + "/.handymenu.conf",
            verbose
        )

    coloring("Etape 2 : Migration des comptes personnalisés", True)
    custom_sessions = list(
        set(os.listdir("/home/")) 
        - set(sessions) 
        - set(['lost+found'])
    )
    for custom_session in custom_sessions:
        convert_config(
            "/home/" + custom_session + "/.handymenu.conf",
            "/home/" + custom_session + "/.config/handymenu/conf.yaml",
            None,
            verbose
        )
        icon_path = "/usr/share/handymenu/icons/primtuxmenu_icon-prof.png"
        custom_icon_path = "/home/" + custom_session + "/.config/handymenu/menu.png"
        try:
            copyfile(
                icon_path,
                custom_icon_path
            )
            coloring(
                "Copie du fichier {0} dans {1} !".format(
                    icon_path,
                    custom_icon_path
                ), True
            )
        except:
            if verbose:
                traceback.print_exc()
            coloring(
                "Impossible de copier le fichier {0} dans {1} !".format(
                    icon_path,
                    custom_icon_path
                )
            )
    coloring("Migration terminée !", True)
