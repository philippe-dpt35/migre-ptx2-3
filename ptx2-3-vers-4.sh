#!/bin/bash

archive="home.tar"
archive_clc="clc-linux.tar"
themes="themes-fluxbox.tar"
lo_mini="libreoffice-mini.tar"
lo_super="libreoffice-super.tar"
lo_maxi="libreoffice-maxi.tar"
lo_admin="libreoffice-admin.tar"
lo_mini_D8="libreoffice-mini-D8.tar.gz"
lo_super_D8="libreoffice-super-D8.tar.gz"
lo_maxi_D8="libreoffice-maxi-D8.tar.gz"
lo_admin_D8="libreoffice-admin-D8.tar.gz"
chemin_toolbar=".config/libreoffice/4/user/config/soffice.cfg/modules/swriter/toolbar"
md5home="395717f8160438092d3e18296834e92e  home.tar"
md5clc="824a4b3bc7f88de3fe1c77ad6b56710b  clc-linux.tar"
#version_requise="8 (jessie)"
#archi_requise="i686"
primtux="01-mini
02-super
03-maxi"

# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/ptx2-3-vers-4.log"
if ! [ -e "$fichierlog" ]
	then > "$fichierlog"
fi
date >> "$fichierlog"
exec 2>>"$fichierlog"

# Suppression de la référence au dépôt Primtux2-Lubuntu
sed -i '/PrimTux-Lubuntu/d' /etc/apt/sources.list.d/primtux.list

apt-get update && apt-get dist-upgrade

# Vérifie l'OS
archi=$(uname -m)
version=$(cat /etc/os-release | grep ^VERSION= | cut -d"\"" -f2)
id=$(cat /etc/os-release | grep ^ID= | cut -d"=" -f2)
sessions=$(grep -E "01-mini|02-super|03-maxi" /etc/passwd | cut -d":" -f1)

#if [ "$archi" != "$archi_requise" ] || [ "$version" != "$version_requise" ] || [ "$sessions" != "$primtux" ]
if [ "$sessions" != "$primtux" ]
   then  echo "La version de votre système d'exploitation ne
correspond pas à celles susceptibles de recevoir
cette mise à jour."
        exit 0
fi

# vérifie la présence et l'intégrité des archives
if ! [ -e "$archive" ] || ! [ -e "$archive_clc" ]
   then echo "Le script va s'interrompre car il manque une archive
des fichiers nécessaires à la mise à jour."
        exit 0
fi

md5a=$(md5sum "$archive")
md5b=$(md5sum "$archive_clc")
if [ "$md5a" != "$md5home" ] || [ "$md5b" != "$md5clc" ]
   then  echo "Une archive de fichiers est corrompue.
Vous devrez peut-être refaire le téléchargement."
        exit 0
fi

# Demande confirmation de la mise à jour
echo "Votre version de PrimTux va être mise à jour pour
bénéficier de l'apparence de la nouvelle PrimTux4.
Confirmez-vous l'opération ? (O/N défaut N)"
read confirmation

if [ "$confirmation" != "O" ] && [ "$confirmation" != "o" ]
   then exit 0
fi

# Nettoyage des fichiers de configuration obsolètes
rm /home/administrateur/.config/xfce4/panel/launcher-8/15250977438.desktop
rm /home/03-maxi/.config/xfce4/panel/launcher-15/14502846451.desktop
rm /home/02-super/.config/xfce4/panel/launcher-15/14502846451.desktop
rm -rf /home/01-mini/.local/share/applications/*
rm -rf /home/02-super/.local/share/applications/*
rm -rf /home/03-maxi/.local/share/applications/*
rm -rf /home/administrateur/.local/share/applications/*
rm -rf /home/01-mini/.config/libreoffice
rm -rf /home/02-super/.config/libreoffice
rm -rf /home/03-maxi/.config/libreoffice
rm -rf /home/administrateur/.config/libreoffice

# Conversion des fichiers de configuration des handymenus
python handymenuMigration.py

# Installation des fichiers de la nouvelle configuration
tar xvf "$archive" -C "/"
tar xvf "$archive_clc" -C "/home/02-super"
tar xvf "$archive_clc" -C "/home/03-maxi"
tar xvf "$archive_clc" -C "/home/administrateur"
tar xvf "$themes" -C "/usr/share/fluxbox/styles"
cp gspeech.png /usr/share/pixmaps/gspeech.png

# Libreoffice des écoles
if [ "$version" = "8 (jessie)" ]
   then tar xvf "$lo_mini_D8" -C "/home/01-mini/.config"
        tar xvf "$lo_super_D8" -C "/home/02-super/.config"
        tar xvf "$lo_maxi_D8" -C "/home/03-maxi/.config"
        tar xvf "$lo_admin_D8" -C "/home/administrateur/.config"
   else tar xvf "$lo_mini" -C "/home/01-mini/.config"
        tar xvf "$lo_super" -C "/home/02-super/.config"
        tar xvf "$lo_maxi" -C "/home/03-maxi/.config"
        tar xvf "$lo_admin" -C "/home/administrateur/.config"
fi

# Harmonisation des paquets
sudo apt-get remove -y --purge handymenu-primtux2 handymenu-mini-primtux2 handymenu-maxi-primtux2 handymenu-super-primtux2 qalculate-gtk
apt-get install -y handymenu thunderbird thunderbird-l10n-fr gnome-calculator geany mothsart-wallpapers-primtux gspeech xfce4-notifyd gir1.2-gstreamer-1.0

# Rétablissement de la propriété de chaque home
chown -R 01-mini:01-mini /home/01-mini
chown -R 02-super:02-super /home/02-super
chown -R 03-maxi:03-maxi /home/03-maxi

# On protège certains fichiers
chown -R administrateur:administrateur /home/01-mini/.config/lxpanel
chown -R administrateur:administrateur /home/01-mini/.config/xfce4
chown -R administrateur:administrateur /home/02-super/.config/lxpanel
chown -R administrateur:administrateur /home/02-super/.config/xfce4
chown -R administrateur:administrateur /home/03-maxi/.config/lxpanel
chown -R administrateur:administrateur /home/03-maxi/.config/xfce4
chown -R administrateur:administrateur /home/01-mini/.config/libfm
chown -R administrateur:administrateur /home/02-super/.config/libfm
chown -R administrateur:administrateur /home/03-maxi/.config/libfm
chown -R administrateur:administrateur /home/01-mini/.fluxbox/startup
chown -R administrateur:administrateur /home/02-super/.fluxbox/startup
chown -R administrateur:administrateur /home/03-maxi/.fluxbox/startup
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/Options
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/globicons
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/menus2
chown administrateur:administrateur /home/01-mini/.config/rox.sourceforge.net/ROX-Filer/panels
chown administrateur:administrateur /home/02-super/.config/rox.sourceforge.net/ROX-Filer/panels
chown administrateur:administrateur /home/03-maxi/.config/rox.sourceforge.net/ROX-Filer/panels
chmod -R 777 /home/02-super/abuledu-primtux/abuledu-aller
chmod -R 777 /home/03-maxi/abuledu-primtux/abuledu-aller

chmod -R -w /home/{01-mini,02-super,03-maxi,administrateur}/"$chemin_toolbar"
chmod +w /home/{01-mini,02-super,03-maxi,administrateur}/"$chemin_toolbar"/standardbar.xml
chmod +w /home/{01-mini,02-super,03-maxi,administrateur}/"$chemin_toolbar"/textobjectbar.xml

# Indication de version PrimTux
if ! [ -e "/etc/primtux_version" ]
   then > /etc/primtux_version
      if [ "$version" = "8 (jessie)" ]
         then case "$id" in
                  debian) if [ "$archi" = "i686" ]
                             then echo "Migration PrimTux2 vers PrimTux4 Debian8 i686" > /etc/primtux_version
                                  else if [ "$archi" = "x86_64" ]
                                          then echo "Migration PrimTux2 vers PrimTux4 Debian8 amd64" > /etc/primtux_version
                                       fi
                          fi
                          ;;
                  raspbian) echo "Migration PrimTux2 RPi vers PrimTux4 Raspbian8" > /etc/primtux_version;;
              esac
         else if [ "$version" = "9 (stretch)" ]
                 then case "$id" in
                         debian) if [ "$archi" = "i686" ]
                                    then echo "Migration PrimTux3 vers PrimTux4 Debian9 i686 DG" > /etc/primtux_version
                                    else if [ "$archi" = "x86_64" ]
                                          then echo "Migration PrimTux3 vers PrimTux4 Debian9 amd64 DG" > /etc/primtux_version
                                         fi
                                 fi
                                 ;;
                         raspbian) echo "Migration PrimTux3 RPi vers PrimTux4 Raspbian9 DG" > /etc/primtux_version;;
                      esac
              fi
      fi
fi

apt-get update && apt-get dist-upgrade

exit 0
